# frozen_string_literal: true

module CustomTokenErrorResponse
  def body
    {
      errors: {
        field_name: 'base',
        message: I18n.t('devise.failure.invalid', authentication_keys: User.authentication_keys.join('/'))
      }
    }
    # or merge with existing values by
    # super.merge({key: value})
  end
end
