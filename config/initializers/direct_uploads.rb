require 'active_storage/direct_uploads_controller'
require 'active_storage/current'
require 'action_controller/renderer'

class ActiveStorage::DirectUploadsController
  protect_from_forgery with: :null_session
end

ActionController::Renderer.const_set('DEFAULTS', {
  http_host: ENV['API_HOST'],
  https: false,
  method: "get",
  script_name: "",
  input: ""
}.freeze
)