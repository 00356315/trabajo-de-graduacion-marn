# frozen_string_literal: true

Doorkeeper.configure do
  # Change the ORM that doorkeeper will use (needs plugins)
  orm :active_record

  # This block will be called to check whether the resource owner is authenticated or not.
  resource_owner_authenticator do
    raise "Please configure doorkeeper resource_owner_authenticator block located in #{__FILE__}"
    # Put your resource owner authentication logic here.
    # Example implementation:
    #   User.find_by_id(session[:user_id]) || redirect_to(new_user_session_url)
  end

  resource_owner_from_credentials do |routes|
    user = User.find_for_database_authentication(:email => params[:email])
    # user = user.where('deleted_at IS NULL')
    if user && user.kept? && user.valid_for_authentication? { user.valid_password?(params[:password]) }
      user
    end
  end

  grant_flows %w(password)


   # token expiration
   access_token_expires_in 2.hours


   # use_refresh_token
   use_refresh_token revoke_on_refresh: true
 
   # just for testing, delete later
   skip_authorization do
     true
   end 
end

require "#{Rails.root}/lib/custom_token_response"
require "#{Rails.root}/lib/custom_token_error_response"
Doorkeeper::OAuth::TokenResponse.send :prepend, CustomTokenResponse
Doorkeeper::OAuth::ErrorResponse.send :prepend, CustomTokenErrorResponse
require Doorkeeper::Engine.root.join('lib/doorkeeper/orm/active_record/access_token.rb')
Doorkeeper::AccessToken.class_eval do
  after_save :revoke_old_refresh_token!

  def revoke_old_refresh_token!
    self.class.all.where(refresh_token: previous_refresh_token).update_all(revoked_at: Time.zone.now,expires_in: 0)
  end
end
