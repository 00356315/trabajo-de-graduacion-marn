# frozen_string_literal: true

Rails.application.routes.draw do
  default_url_options :host => ENV['API_HOST']
  use_doorkeeper
  namespace :api do
    namespace :v1 do
      devise_for :users, controllers: {passwords: "api/v1/passwords"}
      namespace :admin do
        resources :categories
        resources :display_categories
        resources :posts
        post '/posts/prepare_blob_metadata', to: 'posts#prepare_blob_metadata'
      end
      resources :devices
      resources :categories, only: %i[index show]
      resources :display_categories, only: %i[index show]
      resources :posts, only: %i[index show] do
        post '/like', to: 'posts#like'
      end
      resources :users, only: [:create]
      resources :category_subscriptions
      get '/me', to: 'users#me'
      put '/me', to: 'users#update'
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
