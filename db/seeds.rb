# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# Category.create(name: 'Oceanografia', slug: 'oceanografia')
# Category.create(name: 'Sismologia', slug: 'sismografia')
# Category.create(name: 'Meteorologia', slug: 'meteorologia')
#
# DisplayCategory.create(name: 'Noticias relevantes', slug: 'noticias-relevantes')
# DisplayCategory.create(name: 'Ultima hora', slug: 'ultima-hora')
# DisplayCategory.create(name: 'Posts tecnicos', slug: 'posts-tecnicos')

User.create(email: "test@mail.com", password: 'Passwordtest123!')
Profile.create(first_name: 'admin', last_name: 'admin', user_profile_type: :technical_user)