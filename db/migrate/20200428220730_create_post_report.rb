class CreatePostReport < ActiveRecord::Migration[6.0]
  def change
    create_table :post_reports do |t|
      t.references :post, foreign_key: true
      t.references :report, foreign_key: true
    end
  end
end
