class CreateCategorySubscription < ActiveRecord::Migration[6.0]
  def change
    create_table :category_subscriptions do |t|
      t.references :user, foreign_key: true
      t.references :subscriptable, polymorphic: true, index: {name: 'index_category_subs_on_subst_and_id'}
      t.boolean :notifications_on
      t.timestamps
    end
  end
end
