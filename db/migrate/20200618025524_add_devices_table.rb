class AddDevicesTable < ActiveRecord::Migration[6.0]
  def change
    create_table :devices do |t|
      t.string :instance_token_id
      t.references :user, foreign_key: true
    end
  end
end
