class AddDisplayFieldToDisplayCategories < ActiveRecord::Migration[6.0]
  def change
    add_column :display_categories, :display_to_all, :boolean, defaul: false
  end
end
