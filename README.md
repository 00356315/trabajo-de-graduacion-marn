# README

* Ruby version

  2.7.0


* Setup
  - Install the dependencies:
    - Postgresql:
    ```
    sudo apt update
    sudo apt install postgresql postgresql-contrib
    ```
    - Imagemagick:
    ```
    # Prepare installation
    sudo apt update
    sudo apt-get install build-essential

    # Download files
    wget https://www.imagemagick.org/download/ImageMagick.tar.gz
    tar xvzf ImageMagick.tar.gz
    cd ImageMagick-[change to your version]

    # Installation
    ./configure
    make
    sudo make install
    sudo ldconfig /usr/local/lib
    ```
    - Ruby with rbenv:
    ```
    # Rbenv
    git clone https://github.com/rbenv/rbenv.git ~/.rbenv
    echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(rbenv init -)"' >> ~/.bashrc
    exec $SHELL
    git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build

    # Ruby 2.7.0
    rbenv install 2.7.0
    rbenv global 2.7.0
    ```
    - Bundler:
    ```
    gem install bundler
    rbenv rehash
    ```
    - Project:
     Clone the repository and run bundle install to install all the Ruby Gems needed.


* System dependencies

  Ruby (https://www.ruby-lang.org/en/downloads/), Bundler (https://bundler.io/), Postgresql (https://www.postgresql.org/download/), A Redis server if you want to offload the workers to a sidekiq queue (https://redis.io/download), imagemagick to proccess images (https://imagemagick.org/script/download.php).


* Configuration

  Setup an environment file (.env) or configure the environment variables in the operating system:
  ```
  FCM_SERVER_KEY= (FCM server key from firebase)
  WEB_API_KEY= (Firebase web api key)
  DB_USER=
  DB_PASSWORD=
  MAIL_SENDER= (email address to send emails from)
  API_HOST=localhost:3000 (API host, localhost:3000 for development only)
  SMTP_USERNAME= (SMTP configuration)
  SMTP_PASSWORD= (SMTP configuration)
  SMTP_PORT= (SMTP configuration)
  SMTP_ADDRESS= (SMTP configuration)
  PASSWORD_RECOVERY_URL= (Front end url that will handle the password recovery flow)
  ```

* Database creation

  Provide the configuration for the Postgresql connection in the .env file.
  After having all your variables set, run:
  ```
  bundle exec rails db:create
  bundle exec rails db:migrate
  bundle exec rails db:seed # Open the seeds.rb file and change the admin password and email to the desired ones for the default admin before running this command
  ```

* Running the server:
  In order to run the server you must execute: `bundle exec rails s`. This will start a server listening on `port 3000` by default.


* Services (job queues, cache servers, search engines, etc.)

  If you want to change the default queue for tasks, edit the environment.rb file from which you would like the changes to be performed (development, production, test) and add:
  ```
  config.active_job.queue_adapter = :sidekiq
  ```
  and add to the .env file the variables:
  REDIS_URL

To configure the configurations you  must create a new Firebase Application and generate a server key for FCM. You can read more here: https://firebase.google.com/docs/cloud-messaging/concept-options

Remember to edit the file notify_users.rb for any changes in the messages

The mailer templates can be found in the templates folder. Currently only the reset password template is being used.