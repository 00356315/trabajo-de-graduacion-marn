class Api::V1::PasswordsController < Devise::PasswordsController
  skip_before_action :doorkeeper_authorize!

  def create
    result = Concepts::User::Operation::ResetPassword.call(params: params)
    render json: nil, status: :created
  end

  def update
    result = Concepts::User::Operation::UpdatePassword.call(params: params)
    render json: result[:response], status: :ok
  end
end