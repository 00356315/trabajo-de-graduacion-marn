# frozen_string_literal: true

module Api
  module V1
    class CategorySubscriptionsController < ApplicationController
      def index
        data = params[:data] || {}
        params[:data] = data.merge(user_id: current_user.id)
        result = Concepts::CategorySubscription::Operation::List.call(
          params: params
        )
        render json: result[:response], status: :ok
      end

      def show
        data = params[:data] || {}
        params[:data] = data.merge(user_id: current_user.id)
        result = Concepts::CategorySubscription::Operation::Find.call(
          params: params
        )
        render json: result[:response], status: :ok
      end

      def create
        data = params[:data] || {}
        params[:data] = data.merge(user_id: current_user.id)
        result = Concepts::CategorySubscription::Operation::Create.call(
          params: params
        )
        render json: result[:response], status: :created
      end

      def update
        data = params[:data] || {}
        params[:data] = data.merge(user_id: current_user.id)
        result = Concepts::CategorySubscription::Operation::Update.call(
          params: params
        )
        render json: result[:response], status: :ok
      end

      def delete
        data = params[:data] || {}
        params[:data] = data.merge(user_id: current_user.id)
        result = Concepts::CategorySubscription::Operation::Delete.call(
          params: params
        )
        render json: result[:response], status: :no_content
      end
    end
  end
end
