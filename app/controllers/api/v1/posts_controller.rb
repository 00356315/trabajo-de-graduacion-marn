# frozen_string_literal: true

module Api
  module V1
    class PostsController < ApplicationController
      skip_before_action :doorkeeper_authorize!, except: [:like]

      def index
        result = Concepts::Post::Operation::List.call(params: params)
        render json: result[:response], status: :ok
      end
      def show
        result = Concepts::Post::Operation::Find.call(params: params)
        render json: result[:response], status: :ok
      end

      def like
        result = Concepts::Like::Operation::Toggle.call(
          params: params.merge(
            user_id: current_user.id
          )
        )
        render json: result[:response], status: :ok
      end
    end
  end
end
