# frozen_string_literal: true

module Api
  module V1
    class DevicesController < ApplicationController
      def create
        result = Concepts::Device::Operation::Create.call(params: params)
        render json: result[:response], status: :created
      end
    end
  end
end
