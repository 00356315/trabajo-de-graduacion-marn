# frozen_string_literal: true

module Api
  module V1
    class CategoriesController < ApplicationController
      skip_before_action :doorkeeper_authorize!

      def index
        result = Concepts::Category::Operation::List.call(params: params)
        render json: result[:response], status: :ok
      end

      def show
        result = Concepts::Category::Operation::Find.call(params: params)
        render json: result[:response], status: :ok
      end
    end
  end
end
