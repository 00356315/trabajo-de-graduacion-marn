# frozen_string_literal: true

module Api
  module V1
    class DisplayCategoriesController < ApplicationController
      skip_before_action :doorkeeper_authorize!

      def index
        result = Concepts::DisplayCategory::Operation::List.call(params: params)
        render json: result[:response], status: :ok
      end

      def show
        result = Concepts::DisplayCategory::Operation::Find.call(params: params)
        render json: result[:response], status: :ok
      end
    end
  end
end
