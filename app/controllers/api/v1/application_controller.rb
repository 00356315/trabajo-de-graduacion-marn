# frozen_string_literal: true

module Api
  module V1
    class ApplicationController < Api::ApplicationController
      include Pundit

      before_action :doorkeeper_authorize!
      before_action :authorize_request!
      before_action :require_data!, only: [:create, :update]
      before_action :configure_permitted_parameters, if: :devise_controller?

      def configure_permitted_parameters
        added_attrs = %i[email first_name last_name]
        devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
        devise_parameter_sanitizer.permit :account_update, keys: added_attrs
      end

      def current_resource_owner
        User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
      end

      def require_data!
        return unless params[:data].blank?
        raise CustomError.new({ base: ['Please send your parameters inside a data node']}, 400)
      end

        def authorize_request!; end

      def current_user
        @current_user ||= current_resource_owner
      end
    end
  end
end
