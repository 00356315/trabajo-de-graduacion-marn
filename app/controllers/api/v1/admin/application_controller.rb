# frozen_string_literal: true

module Api
  module V1
    module Admin
      class ApplicationController < Api::V1::ApplicationController
        def authorize(record, query = nil)
          super([:api, :v1, :admin, record], query)
        end
      end
    end
  end
end
