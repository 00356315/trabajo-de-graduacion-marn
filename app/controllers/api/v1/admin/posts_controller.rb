# frozen_string_literal: true

module Api
  module V1
    module Admin
      class PostsController < ApplicationController
        def index
          result = Concepts::Post::Operation::List.call(params: params)
          render json: result[:response], status: :ok
        end

        def show
          result = Concepts::Post::Operation::Find.call(params: params)
          render json: result[:response], status: :ok
        end

        def create
          result = Concepts::Post::Operation::Create.call(params: params)
          render json: result[:response], status: :created
        end

        def update
          result = Concepts::Post::Operation::Update.call(params: params)
          render json: result[:response], status: :ok
        end

        def delete
          result = Concepts::Post::Operation::Delete.call(params: params)
          render json: result[:response], status: :no_content
        end

        def prepare_blob_metadata
          result = Concepts::Post::Operation::PrepareMetadata.call(params: params)
          render json: result[:response], status: :ok
        end

        private

        def authorize_request!
          authorize Post
        end
      end
    end
  end
end
