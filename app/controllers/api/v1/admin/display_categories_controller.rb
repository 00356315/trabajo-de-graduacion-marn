# frozen_string_literal: true

module Api
  module V1
    module Admin
      class DisplayCategoriesController < ApplicationController
        def index
          result = Concepts::DisplayCategory::Operation::List.call(params: params)
          render json: result[:response], status: :ok
        end

        def show
          result = Concepts::DisplayCategory::Operation::Find.call(params: params)
          render json: result[:response], status: :ok
        end

        def create
          result = Concepts::DisplayCategory::Operation::Create.call(params: params)
          render json: result[:response], status: :created
        end

        def update
          result = Concepts::DisplayCategory::Operation::Update.call(params: params)
          render json: result[:response], status: :ok
        end

        def delete
          result = Concepts::DisplayCategory::Operation::Delete.call(params: params)
          render json: result[:response], status: :no_content
        end

        private

        def authorize_request!
          authorize DisplayCategory
        end
      end
    end
  end
end
