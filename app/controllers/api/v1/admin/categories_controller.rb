# frozen_string_literal: true

module Api
  module V1
    module Admin
      class CategoriesController < ApplicationController
        def index
          result = Concepts::Category::Operation::List.call(params: params)
          render json: result[:response], status: :ok
        end

        def show
          result = Concepts::Category::Operation::Find.call(params: params)
          render json: result[:response], status: :ok
        end

        def create
          result = Concepts::Category::Operation::Create.call(params: params)
          render json: result[:response], status: :created
        end

        def update
          result = Concepts::Category::Operation::Update.call(params: params)
          render json: result[:response], status: :ok
        end

        def delete
          result = Concepts::Category::Operation::Delete.call(params: params)
          render json: result[:response], status: :no_content
        end

        private

        def authorize_request!
          authorize Category
        end
      end
    end
  end
end
