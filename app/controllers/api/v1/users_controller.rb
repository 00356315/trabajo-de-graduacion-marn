# frozen_string_literal: true

module Api
  module V1
    class UsersController < ApplicationController
      skip_before_action :doorkeeper_authorize!, only: :create

      def create
        result = Concepts::User::Operation::Create.call(params: params)
        render json: result[:response], status: :created
      end

      def me
        result = Concepts::User::Operation::Find.call(params: params.merge(id: current_user.id))
        render json: result[:response], status: :ok
      end

      def update
        result = Concepts::User::Operation::Update.call(params: params.merge(id: current_user.id))
        render json: result[:response], status: :ok
      end
    end
  end
end
