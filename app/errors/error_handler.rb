# frozen_string_literal: true

module ErrorHandler
  def self.included(clazz)
    clazz.class_eval do
      rescue_from StandardError do |e|
        if e.class.name.start_with?('Pagy::') # pagination errors
          respond({ pagination: [e.to_s] }, 422)
        else
          respond({ base: [e.to_s] }, 500) # other errors
        end
      end
      rescue_from ActiveRecord::RecordNotFound do |e|
        respond({ base: ['record not found'] }, 404)
      end
      rescue_from CustomError do |e|
        respond(e.errors, e.status)
      end
      rescue_from Pundit::NotAuthorizedError do |e|
        respond({ base: e.to_s }, 401)
      end
    end
  end
  def errors_array(errors)
    errors = errors.messages if errors.respond_to? :messages
    errors_a = []
    errors.each do |e, v|
      v = [v].flatten
      v.each do |message|
        json = { field_name: e, message: message }
        errors_a << json
      end
    end
    errors_a
  end
  private
  def respond(errors, status)
    body = { errors: errors_array(errors) }
    render json: body, status: status
  end
end