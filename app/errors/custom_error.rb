# frozen_string_literal: true

# class CustomError

class CustomError < StandardError
  attr_reader :status, :errors, :message
  def initialize(errors, status)
    @errors = errors || []
    @status = status || :unprocessable_entity
  end
end
