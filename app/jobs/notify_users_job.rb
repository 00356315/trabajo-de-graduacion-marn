# frozen_string_literal: true

class NotifyUsersJob < ApplicationJob
  queue_as :default

  def perform(user_ids, title)
    registration_ids = ::Device.where(user_id: user_ids).pluck(:instance_token_id)
    fcm = FCM.new(ENV.fetch('FCM_SERVER_KEY', ''))
    options = { "notification": {
      "title": title,
      "body": 'Hay un nuevo post publicado!'
    } }
    response = fcm.send(registration_ids, options)
  end
end
