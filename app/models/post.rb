class Post < ApplicationRecord
  has_many :likes
  has_many :post_reports
  has_many :reports, through: :post_reports
  belongs_to :category
  belongs_to :display_category
  has_rich_text :content
  enum post_type: [:news, :other]
end
