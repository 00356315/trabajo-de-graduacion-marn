class Category < ApplicationRecord
  has_many :category_subscriptions, as: :subscriptable
  has_many :users, through: :category_subscriptions
  has_many :posts
  has_one_attached :image

  def image_url
    if image.attached?
      Rails.application.routes.url_helpers.rails_blob_url(image)
    end
  end

end
