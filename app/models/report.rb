class Report < ApplicationRecord
  has_many :posts, through: :post_reports
  has_many :reports

end
