class Profile < ApplicationRecord
  belongs_to :user
  enum user_profile_type: [:standard_user, :technical_user]
end
