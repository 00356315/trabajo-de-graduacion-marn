# frozen_string_literal: true

class User < ApplicationRecord
  include Discard::Model
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_one :profile
  has_many :likes
  has_many :devices
  has_many :category_subscriptions
  has_many :categories, through: :category_subscriptions, source: :subscriptable, source_type: 'Category'
  has_many :display_categories, through: :category_subscriptions, source: :subscriptable, source_type: 'DisplayCategory'
end
