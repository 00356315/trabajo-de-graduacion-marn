# frozen_string_literal: true

module Concepts
  module Category
    module Operation
      # Create Post operation
      class Find < ::Base::Operation::Find
        def setup!(ctx, **)
          ctx[:model_class] = ::Category
          ctx[:presenter_class] = Presenter::CategoryPresenter
        end
      end
    end
  end
end
