# frozen_string_literal: true

module Concepts
  module Category
    module Operation
      # Create Post operation
      class List < ::Base::Operation::List
        def setup!(ctx, **)
          ctx[:model_class] = ::Category
          ctx[:presenter_class] = Presenter::CategoryPresenter
        end
      end
    end
  end
end
