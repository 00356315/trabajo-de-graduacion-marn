# frozen_string_literal: true

module Concepts
  module Category
    module Operation
      # Create Post operation
      class Update < ::Base::Operation::Update
        pass :set_image!, after: :update_model!
        def setup!(ctx, **)
          ctx[:model_class] = ::Category
          ctx[:contract_class] = Contract::Base
          ctx[:presenter_class] = Presenter::CategoryPresenter
        end

        def set_image!(ctx, params:, model:, **)
          unless params[:data][:image].nil?
            model.image.attach(
              io: params[:data][:image].tempfile,
              filename: params[:data][:image].original_filename,
              content_type: params[:data][:image].content_type
            )
          end
        end
      end
    end
  end
end
