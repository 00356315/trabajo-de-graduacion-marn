# frozen_string_literal: true

module Concepts
  module Category
    module Operation
      # Create Post operation
      class Delete < ::Base::Operation::Delete
        def setup!(ctx, **)
          ctx[:model_class] = ::Category
        end
      end
    end
  end
end
