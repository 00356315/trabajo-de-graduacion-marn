# frozen_string_literal: true

module Concepts
  module Category
    module Operation
      # Create Post operation
      class Create < ::Base::Operation::Create
        pass :set_image!, after: :create_model!
        def setup!(ctx, **)
          ctx[:model_class] = ::Category
          ctx[:contract_class] = Contract::Create
          ctx[:presenter_class] = Presenter::CategoryPresenter
        end

        def set_image!(ctx, params:, model:, **)
          unless params[:data][:image].nil?
            model.image.attach(
              io: params[:data][:image].tempfile,
              filename: params[:data][:image].original_filename,
              content_type: params[:data][:image].content_type
            )
          end
        end
      end
    end
  end
end
