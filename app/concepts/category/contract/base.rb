# frozen_string_literal: true

module Concepts
  module Category
    module Contract
      class Base < Reform::Form
        property :name
        property :slug
        property :image, virtual: true

        validates :name, presence: true
        validates :slug, presence: true

        validate :image do
          unless image.nil?
            errors.add(:image, 'Invalid image format') unless ['image/jpeg', 'image/png', 'image/jpg'].include?(image.content_type)
          end
        end
      end
    end
  end
end
