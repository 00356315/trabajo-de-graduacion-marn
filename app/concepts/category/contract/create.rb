# frozen_string_literal: true

module Concepts
  module Category
    module Contract
      class Create < Base
        validates :image, presence: true
      end
    end
  end
end
