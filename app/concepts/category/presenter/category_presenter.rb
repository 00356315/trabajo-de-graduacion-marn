module Concepts
  module Category
    module Presenter
      class CategoryPresenter < ::Base::Presenter::BasePresenter
        property :id
        property :name
        property :slug
        property :image_url
        property :created_at
        property :updated_at
      end
    end
  end
end