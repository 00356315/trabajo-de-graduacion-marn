module Concepts
  module Profile
    module Presenter
      class ProfilePresenter < ::Base::Presenter::BasePresenter
        property :first_name
        property :last_name
        property :user_profile_type
      end
    end
  end
end