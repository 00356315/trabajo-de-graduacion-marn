module Concepts
  module User
    module Presenter
      class UserPresenter < ::Base::Presenter::BasePresenter
        property :id
        property :email
        property :profile, class: Profile, decorator: Profile::Presenter::ProfilePresenter
      end
    end
  end
end