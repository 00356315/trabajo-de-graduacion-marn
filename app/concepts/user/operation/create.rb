# frozen_string_literal: true

module Concepts
  module User
    module Operation
      # Create Post operation
      class Create < ::Base::Operation::Create
        def setup!(ctx, **)
          ctx[:model_class] = ::User
          ctx[:contract_class] = Contract::Base
          ctx[:presenter_class] = Presenter::UserPresenter
        end
      end
    end
  end
end
