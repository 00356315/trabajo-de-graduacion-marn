# frozen_string_literal: true

module Concepts
  module User
    module Operation
      # Base create operation
      class ResetPassword < Trailblazer::Operation
        pass :find_user!
        pass :send_instructions!

        def find_user!(ctx, **)
          params = ctx[:params]
          ctx[:user] = ::User.find_by!(email: params[:data][:email])
        end

        def send_instructions!(_ctx, user:, **)
          user.send_reset_password_instructions
        end
      end
    end
  end
end
