# frozen_string_literal: true

module Concepts
  module User
    module Operation
      # Base create operation
      class UpdatePassword < Trailblazer::Operation
        pass :set_password!

        def set_password!(ctx, **)
          data = ctx[:params][:data]
          user = ::User.find_by!(reset_password_token: data[:reset_password_token])
          user.password = data[:password]
          user.save
          ctx[:response] = {message: 'Password changed correctly'}
        end
      end
    end
  end
end
