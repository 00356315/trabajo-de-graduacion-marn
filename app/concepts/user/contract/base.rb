# frozen_string_literal: true

module Concepts
  module User
    module Contract
      class Base < Reform::Form
        property :email
        property :password
        property :profile,
		 populate_if_empty: :profile_populator do
        property :first_name
        property :last_name
        property :user_profile_type
        validates :first_name, presence: true
        validates :last_name, presence: true
        validates :user_profile_type, inclusion: {in: ['standard_user', 'technical_user'], message: "Must be in [standard_user, technical_user]"}
     end

        validates :email, format: {with: URI::MailTo::EMAIL_REGEXP}
        validates :password, presence: true
	validates :profile, presence: true

	def profile_populator(options)
	  ::Profile.new
	end
      end
    end
  end
end
