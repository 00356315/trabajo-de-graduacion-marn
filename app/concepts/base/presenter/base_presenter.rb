# frozen_string_literal: true

  module Base
    module Presenter
      class BasePresenter < Representable::Decorator
        include Representable::JSON
      end
    end
  end
