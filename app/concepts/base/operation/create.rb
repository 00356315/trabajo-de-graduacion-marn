# frozen_string_literal: true

  module Base
    module Operation
      # Base create operation
      class Create < Trailblazer::Operation
        pass :setup!
        pass :build_model!
        pass :validate_model!
        pass :create_model!
        pass :generate_response!

        def setup!(_ctx, **)
          raise NotImplementedError
        end

        def build_model!(ctx, model_class:, **)
          ctx[:model] = model_class.new
        end

        def validate_model!(ctx, contract_class:, params:, model:, **)
          ctx[:contract] = contract_class.new(model)
          valid = ctx[:contract].validate(params[:data])
          return if valid

          errors = ctx[:contract].errors
          errors = ctx[:model].errors if errors.blank?
          raise CustomError.new(errors, 422)
        end

        def create_model!(_ctx, contract:, **)
          contract.sync
          saved = contract.model.save
          # contract.errors
          return if saved

          errors = ctx[:contract].errors
          errors = ctx[:model].errors if errors.blank?
          raise CustomError.new(errors, 422)
        end

        def generate_response!(ctx, model:, presenter_class:, **)
          ctx[:response] = { data: presenter_class.new(model).to_hash }
        end
      end
    end
  end
