# frozen_string_literal: true

  module Base
    module Operation
    # Base create operation
      class Update < Trailblazer::Operation
        pass :setup!
        pass :build_model!
        pass :validate_model!
        pass :update_model!
        pass :generate_response!

        def setup!(_ctx, **)
          raise NotImplementedError
        end

        def build_model!(ctx, model_class:, params:, **)
          ctx[:model] = model_class.find(params[:id])
        end

        def validate_model!(ctx, contract_class:, params:, model:, **)
          ctx[:contract] = contract_class.new(model)
          valid = ctx[:contract].validate(params[:data])
          raise CustomError.new(ctx[:contract].errors, 422) unless valid
        end

        def update_model!(_ctx, contract:, **)
          contract.sync
          saved = contract.model.save
          raise CustomError.new(contract.errors, 422) unless saved
        end

        def generate_response!(ctx, model:, presenter_class:, **)
          ctx[:response] = { data: presenter_class.new(model).to_hash }
        end
      end
    end
  end
