# frozen_string_literal: true

  module Base
    module Operation
      # Base Delete operation
      class Find < Trailblazer::Operation
        pass :setup!
        pass :build_model!
        pass :generate_response!

        def setup!(_ctx, **)
          raise NotImplementedError
        end

        def build_model!(ctx, model_class:, params:, **)
          # Not found error handled in controller
          ctx[:model] = model_class.find(params[:id])
        end

        def generate_response!(ctx, model:, presenter_class:, **)
          ctx[:response] = { data: presenter_class.new(model).to_hash }
        end
      end
    end
  end
