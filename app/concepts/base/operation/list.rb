# frozen_string_literal: true

  module Base
    module Operation
      # Base Delete operation
      class List < Trailblazer::Operation
        include Pagy::Backend

        pass :setup!
        pass :build_models!
        pass :filter!
        pass :paginate!
        pass :generate_response!

        def setup!(_ctx, **)
          raise NotImplementedError
        end

        def build_models!(ctx, model_class:, **)
          models = model_class.all
          ctx[:models] = models
        end

        def filter!(ctx, models:, params:, **)
          filter = params.fetch(:filter, {})
          ctx[:models] = models.ransack(filter).result
        end

        def paginate!(ctx, params:, model_class:, models:, **)
          page = params.fetch(:page, 1)
          size = params.fetch(:size, 10)
          unless page.to_i.positive? && size.to_i.positive?
            raise CustomError.new([
                                            page: ['Must be a positive integer'],
                                            size: ['Must be a positive integer']
                                          ], 422)
          end

          _, ctx[:models] = pagy(models, items: size, page: page)
          ctx[:meta] = { page: page.to_i, size: size.to_i, total: model_class.count }
        end

        def generate_response!(ctx, models:, presenter_class:, meta:, **)
          presenter = presenter_class.for_collection.new(models)
          ctx[:response] = { data: presenter.to_hash, meta: meta }
        end
      end
    end
  end
