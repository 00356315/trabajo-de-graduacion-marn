# frozen_string_literal: true

  module Base
    module Operation
    # Base Delete operation
      class Delete < Trailblazer::Operation
        pass :setup!
        pass :build_model!
        pass :delete_model!
        pass :generate_response!

        def setup!(_ctx, **)
          raise NotImplementedError
        end

        def build_model!(ctx, model_class:, params:, **)
          # Not found exception handled in controller
          model = model_class.find(params[:id])
          ctx[:model] = model
        end

        def delete_model!(ctx, model:, **)
          model.destroy!
          ctx[:model] = nil
        end

        def generate_response!(ctx, **)
          ctx[:response] = {}
        end
      end
    end
  end
