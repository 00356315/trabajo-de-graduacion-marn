# frozen_string_literal: true
require 'fcm'

module Concepts
  module Post
    module Operation
      # Create Post operation
      class Create < ::Base::Operation::Create
        pass :notify_users!, after: :generate_response!
        def setup!(ctx, **)
          ctx[:model_class] = ::Post
          ctx[:contract_class] = Contract::Base
          ctx[:presenter_class] = Presenter::PostPresenter
        end

        def notify_users!(ctx, **)
          display_category = ctx[:model].display_category
          category = ctx[:model].category

          user_ids = display_category.users.pluck(:id) + category.users.pluck(:id)
          user_ids.uniq!

          NotifyUsersJob.perform_now(user_ids, ctx[:model].title)
        end
      end
    end
  end
end
