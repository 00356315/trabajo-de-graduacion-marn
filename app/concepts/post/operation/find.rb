# frozen_string_literal: true

module Concepts
  module Post
    module Operation
      # Create Post operation
      class Find < ::Base::Operation::Find
        def setup!(ctx, **)
          ctx[:model_class] = ::Post
          ctx[:presenter_class] = Presenter::PostPresenter
        end
      end
    end
  end
end
