# frozen_string_literal: true

module Concepts
  module Post
    module Operation
      # Create Post operation
      class Delete < ::Base::Operation::Delete
        pass :create_report!, after: :generate_response!

        def setup!(ctx, **)
          ctx[:model_class] = ::Post
        end
      end
    end
  end
end
