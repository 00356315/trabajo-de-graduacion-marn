# frozen_string_literal: true

module Concepts
  module Post
    module Operation
      # Create Post operation
      class Update < ::Base::Operation::Update
        def setup!(ctx, **)
          ctx[:model_class] = ::Post
          ctx[:contract_class] = Contract::Base
          ctx[:presenter_class] = Presenter::PostPresenter
        end
      end
    end
  end
end
