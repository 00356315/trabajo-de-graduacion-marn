# frozen_string_literal: true

module Concepts
  module Post
    module Operation
      class List < ::Base::Operation::List
        pass :sort!, after: :filter!
        def setup!(ctx, **)
          ctx[:model_class] = ::Post
          ctx[:presenter_class] = Presenter::PostPresenter
        end

        def sort!(ctx, models:, **)
          ctx[:models] = models.order(created_at: :desc)
        end
      end
    end
  end
end
