# frozen_string_literal: true

module Concepts
  module Post
    module Operation
      # Base Delete operation
      class PrepareMetadata < Trailblazer::Operation
        pass :prepare_file!
        pass :calculate_checksum!
        pass :generate_response!

        def prepare_file!(ctx, **)
          params = ctx[:params]
          file = params[:file]
          ctx[:file] = file
        end

        def calculate_checksum!(ctx, file:, **)
          checksum = Digest::MD5.file(file).base64digest
          ctx[:checksum] = checksum
        end

        def generate_response!(ctx, **)
          ctx[:response] = {
            checksum: ctx[:checksum]
          }
        end
      end
    end
  end
end
