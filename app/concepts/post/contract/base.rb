# frozen_string_literal: true

module Concepts
  module Post
    module Contract
      class Base < Reform::Form
        property :title
        property :slug
        property :content
        property :category_id
        property :display_category_id
        property :post_type

        validates :post_type, inclusion: {in: ['news', 'other'], message: 'Must be in [news, other]'}
        validates :title, presence: true
        validates :slug, presence: true
        validates :content, presence: true
        validates :category_id, presence: true, numericality: {
          only_integer: true,
          greater_than_or_equal_to: 1
        }
        validates :display_category_id, presence: true, numericality: {
          only_integer: true,
          greater_than_or_equal_to: 1
        }
        validate :category_existance
        validate :display_category_existance

        def category_existance
          return if category_id.blank?

          errors.add('category_id', 'Category must exist') unless ::Category.exists?(category_id)
        end

        def display_category_existance
          return if display_category_id.blank?

          errors.add('display_category_id', 'Category must exist') unless ::DisplayCategory.exists?(display_category_id)
        end
      end
    end
  end
end
