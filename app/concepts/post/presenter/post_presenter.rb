module Concepts
  module Post
    module Presenter
      class PostPresenter < ::Base::Presenter::BasePresenter
        property :id
        property :title
        property :slug
        property :content, exec_context: :decorator
        property :category_name, exec_context: :decorator
        property :display_category_name, exec_context: :decorator
        property :post_type
        property :likes_count, exec_context: :decorator
        property :created_at
        property :updated_at

        def likes_count
          represented.likes.count
        end

        def content
          represented.content.to_s
        end

        def display_category_name
          display_category = represented.display_category
          return display_category.name if display_category
        end

        def category_name
          category = represented.category
          return category.name if category
        end
      end
    end
  end
end