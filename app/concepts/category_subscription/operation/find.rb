# frozen_string_literal: true

module Concepts
  module CategorySubscription
    module Operation
      # Create Post operation
      class Find < ::Base::Operation::Find
        def setup!(ctx, **)
          ctx[:model_class] = ::CategorySubscription
          ctx[:presenter_class] = Presenter::CategorySubscriptionPresenter
        end

        def build_model!(ctx, model_class:, params:, **)
          model = model_class.find_by!(id: params[:id], user_id: params[:data][:user_id])
          ctx[:model] = model
        end
      end
    end
  end
end
