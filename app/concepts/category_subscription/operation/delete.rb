# frozen_string_literal: true

module Concepts
  module CategorySubscription
    module Operation
      # Create Post operation
      class Delete < ::Base::Operation::Delete
        def setup!(ctx, **)
          ctx[:model_class] = ::CategorySubscription
        end

        def build_model!(ctx, model_class:, params:, **)
          model = model_class.find_by!(id: params[:id], user_id: params[:data][:user_id])
          ctx[:model] = model
        end

        def sanitize_params!(_ctx, params:, **)
          constant = params[:subscriptable_type].camelize.constantize
          params[:subscriptable_type] = constant
        rescue NameError => _
          params[:subscriptable_type] = Base
        end
      end
    end
  end
end
