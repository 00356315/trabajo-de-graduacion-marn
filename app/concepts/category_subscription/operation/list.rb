# frozen_string_literal: true

module Concepts
  module CategorySubscription
    module Operation
      # Create Post operation
      class List < ::Base::Operation::List
        def setup!(ctx, **)
          ctx[:model_class] = ::CategorySubscription
          ctx[:presenter_class] = Presenter::CategorySubscriptionPresenter
        end

        def build_models!(ctx, model_class:, params:, **)
          models = model_class.where(user_id: params[:data][:user_id])
          ctx[:models] = models
        end
      end
    end
  end
end
