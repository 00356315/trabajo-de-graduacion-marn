# frozen_string_literal: true

module Concepts
  module CategorySubscription
    module Operation
      # Create Post operation
      class Update < ::Base::Operation::Update
        pass :sanitize_params!, after: :build_model!

        def setup!(ctx, **)
          ctx[:model_class] = ::CategorySubscription
          ctx[:contract_class] = Contract::Base
          ctx[:presenter_class] = Presenter::CategorySubscriptionPresenter
        end

        def sanitize_params!(_ctx, params:, model:, **)
          constant = params[:data][:subscriptable_type].camelize.constantize
          params[:data][:subscriptable_type] = constant
        rescue NameError => _
          params[:data][:subscriptable_type] = model.subscriptable_type.constantize
        end
      end
    end
  end
end
