# frozen_string_literal: true

module Concepts
  module CategorySubscription
    module Contract
      class Base < Reform::Form
        SUBSCRIPTABLES = [::Category, ::DisplayCategory].freeze
        property :user_id
        property :subscriptable_id
        property :subscriptable_type
        property :notifications_on

        validates :user_id, presence: true
        validates :subscriptable_id, presence: true
        validates :subscriptable_type,
                  presence: true,
                  inclusion: {
                    in: SUBSCRIPTABLES,
                    message: 'Must be in [category, display_category]'
                  }
        validate :user_existance
        validate :subscriptable_existance

        def user_existance
          return if user_id.blank?

          errors.add('user_id', 'User must exist') unless ::User.exists?(user_id)
        end

        def subscriptable_existance
          return if subscriptable_type.blank?
          return if subscriptable_id.blank?

          errors.add('subscriptable_id', 'Category or Display Category must exist') unless subscriptable_type.exists?(subscriptable_id)
        end
      end
    end
  end
end
