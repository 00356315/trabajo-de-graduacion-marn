module Concepts
  module CategorySubscription
    module Presenter
      class CategorySubscriptionPresenter < ::Base::Presenter::BasePresenter
        property :id
        property :user
        property :subscriptable
        property :subscriptable_type, exec_context: :decorator
        property :notifications_on
        property :created_at
        property :updated_at

        def subscriptable_type
          represented.subscriptable_type.underscore
        end
      end
    end
  end
end