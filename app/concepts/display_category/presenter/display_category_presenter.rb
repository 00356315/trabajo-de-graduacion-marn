module Concepts
  module DisplayCategory
    module Presenter
      class DisplayCategoryPresenter < ::Base::Presenter::BasePresenter
        property :id
        property :name
        property :slug
        property :display_to_all
        property :created_at
        property :updated_at
      end
    end
  end
end