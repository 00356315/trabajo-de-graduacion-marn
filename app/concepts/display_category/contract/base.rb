# frozen_string_literal: true

module Concepts
  module DisplayCategory
    module Contract
      class Base < Reform::Form
        property :name
        property :slug
        property :display_to_all

        validates :name, presence: true
        validates :slug, presence: true
        validates :display_to_all, inclusion: {in: [true, false]}
      end
    end
  end
end
