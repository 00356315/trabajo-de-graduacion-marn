# frozen_string_literal: true

module Concepts
  module DisplayCategory
    module Operation
      # Create Post operation
      class Update < ::Base::Operation::Update
        def setup!(ctx, **)
          ctx[:model_class] = ::DisplayCategory
          ctx[:contract_class] = Contract::Base
          ctx[:presenter_class] = Presenter::DisplayCategoryPresenter
        end
      end
    end
  end
end
