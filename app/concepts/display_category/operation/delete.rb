# frozen_string_literal: true

module Concepts
  module DisplayCategory
    module Operation
      # Create Post operation
      class Delete < ::Base::Operation::Delete
        def setup!(ctx, **)
          ctx[:model_class] = ::DisplayCategory
        end
      end
    end
  end
end
