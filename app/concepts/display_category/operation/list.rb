# frozen_string_literal: true

module Concepts
  module DisplayCategory
    module Operation
      # Create Post operation
      class List < ::Base::Operation::List
        def setup!(ctx, **)
          ctx[:model_class] = ::DisplayCategory
          ctx[:presenter_class] = Presenter::DisplayCategoryPresenter
        end
      end
    end
  end
end
