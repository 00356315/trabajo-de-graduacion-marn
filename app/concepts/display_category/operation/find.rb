# frozen_string_literal: true

module Concepts
  module DisplayCategory
    module Operation
      # Create Post operation
      class Find < ::Base::Operation::Find
        def setup!(ctx, **)
          ctx[:model_class] = ::DisplayCategory
          ctx[:presenter_class] = Presenter::DisplayCategoryPresenter
        end
      end
    end
  end
end
