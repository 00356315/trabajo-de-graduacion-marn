# frozen_string_literal: true

module Concepts
  module Like
    module Operation
      # Base create operation
      class Toggle < Trailblazer::Operation
        pass :setup!
        pass :build_model!
        pass :validate_model!
        pass :toggle_model!
        pass :generate_response!

        def setup!(ctx, **)
          ctx[:model_class] = ::Like
        end

        def build_model!(ctx, model_class:, params:, **)
          ctx[:model] = model_class.where(
            user_id: params[:user_id],
            post_id: params[:post_id]
          ).first_or_initialize
          ctx[:previously_liked] = ctx[:model].persisted?
        end

        def validate_model!(ctx, model:, **)
          valid = model.valid?
          return if valid

          errors = ctx[:model].errors
          raise CustomError.new(errors, 422)
        end

        def toggle_model!(_ctx, model:, previously_liked:, **)
          action_result = true
          unless previously_liked
            action_result = model.save
          end
          model.destroy if previously_liked
          # contract.errors
          return if action_result

          errors = ctx[:model].errors
          raise CustomError.new(errors, 422)
        end

        def generate_response!(ctx, previously_liked:, **)
          message = previously_liked ? 'disliked' : 'liked'
          ctx[:response] = { data: {message:  "The post was #{message}" } }
        end
      end
    end
  end
end
