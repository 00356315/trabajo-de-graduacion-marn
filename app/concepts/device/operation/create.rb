# frozen_string_literal: true

module Concepts
  module Device
    module Operation
      # Create Post operation
      class Create < ::Base::Operation::Create
        def setup!(ctx, **)
          ctx[:model_class] = ::Device
          ctx[:contract_class] = Contract::Base
          ctx[:presenter_class] = Presenter::DevicePresenter
        end
      end
    end
  end
end
