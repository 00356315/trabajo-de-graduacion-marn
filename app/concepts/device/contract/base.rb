# frozen_string_literal: true

module Concepts
  module Device
    module Contract
      class Base < Reform::Form
        property :instance_token_id
        property :user_id

        validates :user_id, presence: true
        validates :instance_token_id, presence: true
        validate :user_exists

        def user_exists
          return if user_id.blank?

          errors.add('user_id', 'User must exist') unless ::User.exists?(user_id)
        end
      end
    end
  end
end
