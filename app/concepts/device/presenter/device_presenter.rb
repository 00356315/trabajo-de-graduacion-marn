module Concepts
  module Device
    module Presenter
      class DevicePresenter < ::Base::Presenter::BasePresenter
        property :id
        property :user_id
        property :instance_token_id
      end
    end
  end
end