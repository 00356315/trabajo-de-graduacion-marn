# frozen_string_literal: true

module Api
  module V1
    module Admin
      class CategoryPolicy < ApplicationPolicy
        def create?
          user.is_admin?
        end

        def update?
          user.is_admin?
        end

        def delete?
          user.is_admin?
        end

        def index?
          user.is_admin?
        end

        def show?
          user.is_admin?
        end
      end
    end
  end
end
